
set -l HOMEBREW_PREFIX (brew --prefix)

source /Users/ehso/.config/fish/aliases.fish

[ -f $HOMEBREW_PREFIX/share/autojump/autojump.fish ]; and source $HOMEBREW_PREFIX/share/autojump/autojump.fish

if status is-interactive
    # Commands to run in interactive sessions can go here
end

eval "$(pyenv init --path)"

cd ~

starship init fish | source

