# Quick edits
alias ll 'ls -la'
alias ls 'ls -FG'

alias vim='/opt/homebrew/bin/vim'

function lsd -d 'List only directories (in the current dir)'
    ls -d */ | sed -Ee 's,/+$,,'
end

alias gs 'git status'
